import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "../views/Login.vue";
import Service from '@/views/Service.vue'
import Consent from '@/views/Consent.vue'
import VueCookies from 'vue-cookies'

Vue.use(VueRouter)

const routes = [
  { path: "*", redirect: "/login" },
  {
    path: "/login",
    name: "Login",
    component: Login,
    // meta: { user: true }
  },
  {
    path: "/service",
    name: "Service",
    component: Service,
    meta: { user: true }
  },
  {
    path: "/consent",
    name: "Consent",
    component: Consent,
    meta: { user: true }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.guest)) {
    const checkcookie = VueCookies.get('info_login')
    console.log(checkcookie);
    if (!checkcookie) {
      next()
    } else if (checkcookie.role === 'superadmin' || checkcookie.role === 'user' || checkcookie.role === 'viewer') {
      next({ name: 'Service' })
    }
  } else if (to.matched.some(record => record.meta.user)) {
    const checkcookie = VueCookies.get('info_login')
    if (!checkcookie) {
      next({ name: 'Login' })
    } else {
      next()
    }
  } else {
    next()
  }
})
export default router
